var assert = require('assert');
class Producto{
  constructor( nombre,precio,cantidad =0){
      this.precio = precio;            
      this.nombre = nombre; 
      this.cantidad = cantidad;       
  }    

 
} 

class ListaProducto{
  constructor( pagina,productos){
      this.pagina = pagina;            
      this.productos = productos;        
  }    

  actualizarLista(productos){
    this.productos = productos;
  }
  agregarProducto(producto){
    this.productos.push(producto); 
  } 
} 
class Inventario{
  constructor( productos){      
      this.productos = productos;        
  }    

  agregarProducto(producto){
    this.productos.push(producto); 
  } 
} 



const productos = [];

describe('Que los usuarios puedan visualizar el catalogo de productos (de lo que ustedes deseen) paginado.', function () {
  describe('Insertar 10 productos en el arreglo',function(){
    for(let i=0;i<10;i++){
      productos.push([new Producto('Producto ' + (i+1),Math.floor(Math.random() * 101))])
    }
    it('Debe ser un arreglo de 10 productos', function () {
      assert.equal(productos.length,10);
    });
  });

  describe('Consultar pagina de productos (cada pagina tiene 3 productos o menos)',function(){
    const pagina = 3;
    const lista = new ListaProducto(pagina,[]);
    it('Debe ir por 3 productos o menos, pero no nulo', function () {
      const inicial = (pagina*3)-3;
      const final = (pagina*3);
      lista.actualizarLista(productos.slice(inicial,final));
      assert(lista.productos.length<=3&&lista.productos.length>0);
    });
  });
});
/////////////INVENTARIO///////////////
const inventario = new Inventario([]);
describe('El administrador debe poder generar productos y mantener su inventario.', function () {
  describe('Insertar 10 productos en el inventario',function(){
    for(let i=0;i<10;i++){
      inventario.agregarProducto([new Producto('Producto ' + (i+1),Math.floor(Math.random() * 101),Math.floor(Math.random() * 11))]);
    }

  });

  describe('Generar producto nuevo con inventario',function(){  
    describe('Se genera el producto nuevo',function(){
      const productoNuevo = new  Producto('Producto Nuevo',50,10);
      it('El nombre del producto nuevo es "Producto Nuevo"', function () {
        assert.equal(productoNuevo.nombre,"Producto Nuevo");
      });
      it('El precio del producto es 50 pesos', function () {
        assert.equal(productoNuevo.precio,50);
      });
      it('El producto llego con un total de 10 cantidades', function () {
        assert.equal(productoNuevo.cantidad,10);
      });
      describe('Se agrega el producto en el inventario y se valida se agregó',function(){
        inventario.agregarProducto(productoNuevo);
        it('Debe existir el producto agregado en el inventario', function () {
      
          assert(inventario.productos.includes(productoNuevo));
        });
      });
      
    });

 
      
  });
});

